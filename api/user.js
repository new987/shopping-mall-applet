import request from '@/request'

// 登录
export function dyUserLogin (data) {
  return request.post(`/login/dy/${data.code}`, data)
}

// 登录
export function userLogin (data) {
  return request.post('/login/dy/phone', data)
}

// 登录
export function wxUserLogin (data) {
  return request.post('/login/wechat', data)
}

// 更新用户信息
export function updateUserinfo (data) {
  return request.post('/update', data)
}

// 获取用户信息
export function fetchUserInfo (data) {
  return request.post('/info', data)
}

// 获取地址列表
export function fetchAddressList (data) {
	return request.post('/address/list', data)
}

// 保存地址
export function saveAddress (data) {
	return request.post('/address/save', data)
}

// 默认地址
export function defaultAddress (id) {
	return request.post(`/address/def/${id}`)
}

// 删除地址
export function deleteAddr (id) {
	return request.post(`/address/del/${id}`)
}

// 选中订单地址
export function checkedAddressApi (data) {
	return request.post('/order/modify/address', data)
}