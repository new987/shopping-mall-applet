import request from '@/request'

// 订单列表
export function fetchOrderList (data) {
  return request.post('/order/list', data)
}

// 订单详情
export function orderDetailsApi (data) {
  return request.post('/order/info/' + data.orderId, data)
}

// 物流信息
export function orderLogisticsApi (data) {
  return request.post('/order/logistics/' + data.orderId, data)
}

// 删除
export function delOrderApi (data) {
  return request.post('/order/del/' + data.orderId, data)
}

// 确认收货
export function confirmOrderApi (data) {
  return request.post('/order/conf/' + data.orderId, data)
}

// 下单
export function submitOrder (data) {
  return request.post('/pay/order', data)
}


// 测试下单
export function testOrderApi (data) {
  return request.post('/order', data)
}
