import request from '@/request'

// 首页商品分类
export function payApi (data) {
  return request.post('/pay', data)
}

export function checkPayApi (data) {
  return request.post('/pay/check/' + data.orderNo, data)
}