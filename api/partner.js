import request from '@/request'

export const getchAgentInfo = (data) => {
  return request.post('/agent/info/' + data.agentId, data)
}

// 提现
export function withdrawApi (data) {
	return request.post('/agent/wdr', data)
}

export const getchAgentAccount = (data) => {
  return request.post('/agent/account/' + data.openid, data)
}

export const fetchAccountDetails = (data) => {
  return request.post('/agent/income', data)
}

export const getInviteCode = (data) => {
  return request.get('/agent/code/' + data.code, data)
}