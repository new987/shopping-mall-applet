import request from '@/request'

// 首页商品分类
export function fetchRecommandClassify (data) {
  return request.post('/index/rmd', data)
}
// 商品分类
export function fetchGoodClassify (data) {
  return request.post('/goods/class/list', data)
}

// 商品列表
export function fetchGoodList (data) {
  return request.post('/goods/list', data)
}

// 商品详情
export function fetchGoodDetails (data) {
  return request.post(`/goods/info/${data.goodsId}`, data)
}

// sku
export function fetchGoodsSku (data) {
  return request.post('/goods/sku', data)
}

// 收藏
export function likeCollect (data) {
  return request.post(`/collect/like/${data.goodsId}`, data)
}

// 取消收藏
export function cancelCollect (data) {
  return request.post(`/collect/cancel/${data.goodsId}`, data)
}

// 收藏列表
export function fetchCollectList (data) {
  return request.post('/collect/list', data)
}