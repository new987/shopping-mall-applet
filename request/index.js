// export const baseUrl = "http://120.78.175.126:7051/user"
export const baseUrl = "https://mall.haiyunjigo.com/user"
const request = {
	request: (method, url, data, apiConfig) => {
		return new Promise((resolve, reject) => {
			if(!apiConfig  || apiConfig.isLoading !== false) {
				uni.showLoading({
					title: '加载中···'
				})
			}
			uni.request({
				url: baseUrl + url,
				data: data || {},
				method: method,
				header: {
					'content-type': 'application/json',
					token: uni.getStorageSync('token') || ''
				},
				success: (res) => {
					console.log('请求成功', res)
					// if (!res.data.success) return reject(res.data.message)
					if(res.data.code == '401') {
						uni.removeStorageSync('isLogin')
						uni.removeStorageSync('token')
						uni.removeStorageSync('userInfo')
						uni.showModal({
							showCancel: false,
							content: "请登录！",
							success(res) {
								if(res.confirm) uni.navigateTo({url: "/pages/common/login"})
							}
						})
						return reject(res.data.message || res.data.msg)
					}
					if(res.data.code !== 1) {
						uni.showToast({
							icon: "none",
							title: res.data.message || res.data.msg,
							duration: 3000,
						})
						return reject(res.data.message || res.data.msg)
					}
					resolve(res.data.data)
				},
				fail: (err) => {
					console.log(err, '请求失败')
					reject(new Error(err))
				},
				complete: (e) => {
					uni.stopPullDownRefresh()
					// 避免toast弹框一闪而过
					if(e.data.code !== 1 && e.data.code != 401) {
						setTimeout(() => {
							uni.hideLoading()
						}, 3000)
					} else {
						uni.hideLoading()
					}
					// uni.hideLoading()
				}
			})
		})
	},
	get (url, data={}, apiConfig={}) {
		return this.request('GET', url, data, apiConfig)
	},
	post (url, data={}, apiConfig={}) {
		return this.request('POST', url, data, apiConfig)
	}
}

export default request
