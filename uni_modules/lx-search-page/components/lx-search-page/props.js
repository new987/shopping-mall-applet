export default {
	props: {
		// 搜索框样式
		searchInputStyle: {
			type: String,
			default: ""
		},
		// 搜索框提示
		placeholder: {
			type: String,
			default: '请输入搜索内容'
		},
		// 搜索按钮样式
		searchButtonStyle: {
			type: String,
			default: ""
		},
		// 搜索按钮名称
		searchText: {
			type: String,
			default: '搜索'
		},
		// 热门搜索记录
		hotList: {
			type: Array,
			default: []
		},
		// 展示内容
		list: {
			type: Array,
			default: []
		},
		// 显示历史搜索
		showHistory: {
			type: Boolean,
			default: false
		},
		// 显示热门搜索
		showHot: {
			type: Boolean,
			default: false
		},
		// 搜索框提示语显示热门搜索
		showHotPlaceholder: {
			type: Boolean,
			default: false
		},
		// 搜索列表展示内容字段
		checked: {
			type: String,
			default: 'label'
		},
		// 高亮样式
		highlight: {
			type: String,
			default: 'color:#007aff;font-size:4vw'
		}
	}
}