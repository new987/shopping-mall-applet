# lx-search-page

功能巨齐全的搜索页，需要的模块配置即可，开箱即用。
1. 可配置历史搜索和热门搜索
2. 搜索关键字高亮
3. 历史搜索支持单个记录删除和所有记录删除
4. 搜索框提示语，可设置为热门搜索关键词，定时轮换
5. 自定义搜索框、按钮样式
6. 自定义关键字高亮样式
7. 自定义搜索列表展示形式

## 注意：
### 1.该插件依赖 uni-icon 插件，插件市场搜索导入即可
### 2.将插件中的图片复制到   /static/目录下

## 基本用法
导入方法：
1. 直接使用 HBuilderX 导入插件
2. 下载插件 zip ,将 lx-cascade-select 文件夹复制到项目 components 文件夹中，import+components 引入。

使用方式(demo)：
```js
	<template>
		<lx-search-page
			searchInputStyle="border: none;border-radius: 4vw;background-color: aliceblue;"
			searchButtonStyle="border-radius: 4vw;background-color: #fff;color: #007aff;"
			highlight="color:#4cd964;"
			placeholder="你好！"
			searchText="查询"
			checked="name"
			showHot
			showHistory
			showHotPlaceholder
			:list="dataList"
			:hotList="hotList"
			@clear="handleClear"
			@search="handleSearch"
			@detele="handleDelete"
		></lx-search-page>
	</template>
	
	<script>
	export default {
		data() {
			return {
				dataList: [],
				hotList: []
			};
		},
		created() {
			this.hotList = [
				'热门搜索',
				'武功山',
				'长沙',
				'文和友',
				'鼓浪屿',
				'世界之窗',
				'澳门',
				'臭豆腐'
			];
		},
		methods: {
			handleSearch(item) {
				console.log('搜索关键字', item);
	
				let str = '文和友';
				if (str.includes(item)) {
					// 因为展示内容的 key 为'name'，将 checked 的值设为'name',
					// 两者相同才能正常展示搜索列表
					this.dataList = [
						{ name: '文和友', value: '1' },
						{ name: '超级文和友', value: '2' },
						{ name: '文和友和文和友', value: '3' }
					];
				} else {
					this.dataList = [];
				}
			},
			handleDelete() {
				console.log('删除所有历史记录');
			},
			handleClear() {
				console.log('清空搜索框内容');
			}
		}
	};
	</script>
	
```
    


### Attributes, Events and Slot
| 参数				| 类型		| 说明																															| 是否必传	|
|--------			|---		|--------																														|------		|
| list				| Array		| 点击搜索后展示的搜索列表，list:[{label:'', value:''}]																			| 否		|
| checked			| String	| 搜索列表展示内容字段，默认为 'label'																							| 否		|
| hotList			| Array		| 热门搜索列表，结构见使用方法(demo)																							| 否		|
| showHistory		| Boolean	| 显示历史搜索，默认为 false																									| 否		|
| showHot			| Boolean	| 显示热门搜索，默认为 false																									| 否		|
| showHotPlaceholder| Boolean	| 搜索框提示语变成热门搜索关键词，定时轮换，默认为 false。`注意:hotList 必须有值，否则展示 placeholder，开启后 placeholder 无效`| 否		|
| searchInputStyle	| String	| 搜索框样式，书写格式见使用方法(demo)																							| 否		|
| searchButtonStyle	| String	| 搜索按钮样式，书写格式见使用方法(demo)																						| 否		|
| highlight			| String	| 关键字高亮样式，书写格式见使用方法(demo)																						| 否		|
| placeholder		| String	| 搜索框提示语，默认为 "请输入搜索内容"																							| 否		|
| searchText		| String	| 搜索按钮名称，默认为 "搜索"																									| 否		|


| 事件名	| 说明										| 返回值|
|--------	|-------------------------					|-------|
| @clear	| 点击搜索框清空按钮时触发					|		|
| @search	| 点击搜索按钮时触发，返回参数是搜索框内容	| String|
| @detele	| 点击删除所有历史记录时触发				|		|


| 插槽名称	| 说明						|
|---------	|-------------------------	|
| default	| 自定义搜索列表展示形式	|
