// 获取assets静态资源
export const getAssetsFile = (url) => {
  return new URL(`../static/images/${url}`, import.meta.url).href
}

// 复制
export const copy = (text) => {
  uni.setClipboardData({
    data: text,
    success: function () {
      uni.showToast({
        title: '复制成功',
        icon: 'success'
      })
    }
  })
}
